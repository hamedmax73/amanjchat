<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $fillable = ['body'];

    protected $appends = ['selfMessage', 'timeDiff','jdate'];

    public function getJdateAttribute()
    {
        return jdate($this->created_at)->format('d ام ساعت  H:i');
    }

    public function timeDiff($time2, $time1)
    {
        $diff = strtotime($time2) - strtotime($time1);
        if ($diff < 60) {
            return $diff . ' ثانیه قبل';
        } elseif ($diff < 3600) {
            return round($diff / 60, 0, 1) . ' دقیقه قبل';
        } elseif ($diff >= 3660 && $diff < 86400) {
            return round($diff / 3600, 0, 1) . ' ساعت قبل';
        } elseif ($diff > 86400) {
            return round($diff / 86400, 0, 1) . ' روز قبل';
        }
    }

    public function getTimeDiffAttribute()
    {
        $dt = new DateTime();
        $time2=  $dt->format('Y-m-d H:i:s');

        $time1 = $this->created_at;
//        return $time2;
        return $this->timeDiff($time2,$time1);
    }

    public function getSelfMessageAttribute()
    {
        return $this->user_id === auth()->user()->id;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
